package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.shopping.entity.ProductAttrDefineEntity;
import com.ibpd.shopping.entity.ProductAttrValueEntity;
import com.ibpd.shopping.service.product.IProductAttrDefineService;
import com.ibpd.shopping.service.product.ProductAttrDefineServiceImpl;

public class ExtAttrEntity extends ProductAttrValueEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String attributeName;
	private String attributeValue;
	public ExtAttrEntity(ProductAttrValueEntity a){
		try {
			InterfaceUtil.swap(a, this);
				IProductAttrDefineService attrServ=(IProductAttrDefineService) ServiceProxyFactory.getServiceProxy(ProductAttrDefineServiceImpl.class);
				ProductAttrDefineEntity def=attrServ.getEntityById(this.getAttrId());
				this.setAttributeValue(def.getAttrName());
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getAttributeName() {
		return attributeName;
	}
	@Override
	public String toString() {
		if(this.getValue()==null){
			return "{\"type\":\"attribute\",\"id\":\""+this.getId()+"\",\"attrId\":\""+this.getAttrId()+"\",\"attrName\":\""+this.getAttributeName()+"\",\"attrValue\":\""+this.getAttributeValue()+"\",\"value\":\""+this.getValue()+"\"}";
		}else{
			return "{\"type\":\"param\",\"id\":\""+this.getId()+"\",\"attrId\":\""+this.getAttrId()+"\",\"attrName\":\""+this.getAttributeName()+"\",\"value\":\""+this.getValue()+"\"}";
		}
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
}
