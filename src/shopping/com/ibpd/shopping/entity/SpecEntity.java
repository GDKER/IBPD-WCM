package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_spec")
public class SpecEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_productId",nullable=true)
	private Long productId;
	@Column(name="f_specColor",length=45,nullable=true)
	private String specColor;
	@Column(name="f_specSize",length=45,nullable=true)
	private String specSize;
	@Column(name="f_specStock",length=45,nullable=true)
	private String specStock;
	@Column(name="f_specPrice",nullable=true)
	private Float specPrice;
	@Column(name="f_specStatus",length=1,nullable=true)
	private String specStatus="n";
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getSpecColor() {
		return specColor;
	}
	public void setSpecColor(String specColor) {
		this.specColor = specColor;
	}
	public String getSpecSize() {
		return specSize;
	}
	public void setSpecSize(String specSize) {
		this.specSize = specSize;
	}
	public String getSpecStock() {
		return specStock;
	}
	public void setSpecStock(String specStock) {
		this.specStock = specStock;
	}
	public Float getSpecPrice() {
		return specPrice;
	}
	public void setSpecPrice(Float specPrice) {
		this.specPrice = specPrice;
	}
	public String getSpecStatus() {
		return specStatus;
	}
	public void setSpecStatus(String specStatus) {
		this.specStatus = specStatus;
	}
	@Override
	public String toString() {
		JSONArray json=JSONArray.fromObject(this);
		return json.toString().substring(1,json.toString().length()-1);
	}
	
}
