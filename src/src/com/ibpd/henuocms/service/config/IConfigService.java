package com.ibpd.henuocms.service.config;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.ConfigEntity;
import com.ibpd.henuocms.entity.FieldEntity;

public interface IConfigService extends IBaseService<ConfigEntity> {

	void InitSysConfigParams();
}
