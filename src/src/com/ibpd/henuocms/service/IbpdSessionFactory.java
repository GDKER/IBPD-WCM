package com.ibpd.henuocms.service;

import java.io.PrintStream;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.ibpd.henuocms.entity.ModelEntity;
/**
 * 自定义session工厂
 * @author mg by qq:349070443
 * 
 */
public class IbpdSessionFactory
{ 
  private static String CONFIG_FILE_LOCATION = "/hibernateConfig.xml";
  private static final ThreadLocal<Session> threadLocal = new ThreadLocal();
  private static Configuration configuration = new AnnotationConfiguration();
  private static SessionFactory sessionFactory;
  private static String configFile = CONFIG_FILE_LOCATION;

  static {
    try {
      configuration.configure(configFile);
//      configuration.addClass(ModelEntity.class).createMappings().;
//      configuration.getClassMapping("modelEntity");
      sessionFactory = configuration.buildSessionFactory();
    } catch (Exception e) {
      System.err
        .println("%%%% Error Creating SessionFactory %%%%");
      e.printStackTrace();
    }
  }

  public static Session getSession() throws HibernateException
  {
    Session session = (Session)threadLocal.get();

    if ((session == null) || (!session.isOpen())) {
      if (sessionFactory == null) {
        rebuildSessionFactory();
      }
      session = (sessionFactory != null) ? sessionFactory.openSession() : null;
      threadLocal.set(session);
    }

    return session;
  }

  public static void rebuildSessionFactory() {
    try {
      configuration.configure(configFile);
      sessionFactory = configuration.buildSessionFactory();
    } catch (Exception e) {
      System.err
        .println("%%%% Error Creating SessionFactory %%%%");
      e.printStackTrace();
    }
  }

  public static void closeSession() throws HibernateException {
    Session session = (Session)threadLocal.get();
    threadLocal.set(null);

    if (session != null)
      session.close();
  }

  public static SessionFactory getSessionFactory()
  {
    return sessionFactory;
  }
  public static void setConfigFile(String configFile) {
    configFile = configFile;
    sessionFactory = null;
  }

  public static Configuration getConfiguration() {
    return configuration;
  }

  public static void main(String[] args)
  {
  }
}