package com.ibpd.henuocms.service.user;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.UserRoleEntity;

public interface IUserRoleService extends IBaseService<UserRoleEntity> {
	List<UserRoleEntity> getListByRoleId(Long roleId);
	List<Long> getRoleIdByUserId(Long userId);
	void clearRoleUser(Long roleId);
}
 