package com.ibpd.henuocms.service.role;

import java.util.ArrayList;
import java.util.List;

import org.h2.util.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.RolePermissionEntity;
@Transactional
@Service("rolePermissionService")
public class RolePermissionServiceImpl extends BaseServiceImpl<RolePermissionEntity> implements IRolePermissionService {
	public RolePermissionServiceImpl(){
		super();
		this.tableName="RolePermissionEntity";
		this.currentClass=RolePermissionEntity.class;
		this.initOK();
	}

	public List<RolePermissionEntity> getListByRoleId(Long roleId) {
		return getList("from "+getTableName()+" where roleId="+roleId,null);
	}
 
	public void delRolePermission(Long roleId) {
		List<RolePermissionEntity> lst=getListByRoleId(roleId);
		if(lst==null){
			return;
		}
		String ids="";
		for(RolePermissionEntity r:lst){
			deleteByPK(r.getId());
		}
	}

	public List<RolePermissionEntity> getListByRoleIds(String ids) {
		List<RolePermissionEntity> rtnList=new ArrayList<RolePermissionEntity>();
		if(ids==null)
			return rtnList;
		if(ids.trim().length()==0)
			return rtnList;
		String[] idss=ids.split(",");
		String hql="";
		for(String id:idss){
			if(StringUtils.isNumber(id)){
				hql+=" roleId="+id+" or ";
			}
		}
		hql+=" roleId=-99";
		hql="from "+this.getTableName()+" where "+hql;
		return getList(hql,null);
	}
}
