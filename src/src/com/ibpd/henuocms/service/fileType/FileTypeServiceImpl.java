package com.ibpd.henuocms.service.fileType;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.FileTypeEntity;
@Service("fileTypeService")
public class FileTypeServiceImpl extends BaseServiceImpl<FileTypeEntity> implements IFileTypeService {
	public FileTypeServiceImpl(){
		super();
		this.tableName="FileTypeEntity";
		this.currentClass=FileTypeEntity.class;
		this.initOK();
	}
	
}
 