package com.ibpd.henuocms.tlds;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.hsqldb.lib.StringUtil;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.ListSortUtil;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;

public class NodeEachTag extends SimpleBaseTag{
	public static String defaultNodeId="OWNER";
	private Object nodeId=defaultNodeId;
	private String var="node";
	private String orderField="orderByNavigator";
	private String orderType="asc";
	private String pageSize="10";
	private String pageIndex="0";
	private String siteId="currentSite";
	private Integer currentPageSize=10;
	private Integer currentPageIndex=0;
	@Override
	public void doTag() throws JspException, IOException {
		_init();
		init();
		if(getIte()==null){
            return ;
            }
	       while (getIte().hasNext()) {
	            Object obj = getIte().next();
	            getJspContext().setAttribute(var, obj);
	            //输出标签体
	            if(getJspBody()!=null)
	            	getJspBody().invoke(null);
	        }
	}
	private void _init(){
		try{
			if(StringUtil.isEmpty(var)){
				var="node";
			}
			if(StringUtil.isEmpty(orderField)){
				orderField="orderByNavigator";
			}
			if(StringUtil.isEmpty(orderType)){
				orderType="asc";
			}else{
				if(!orderType.toLowerCase().trim().equals("asc") && !orderType.toLowerCase().trim().equals("desc")){
					orderType="asc";
				}
			}
			if(StringUtils.isNumeric(pageSize)){
				currentPageSize=Integer.parseInt(pageSize);
			}else{
				currentPageSize=10;
			}
			if(StringUtils.isNumeric(pageIndex)){
				currentPageIndex=Integer.parseInt(pageIndex);
			}else{
				currentPageIndex=0;
			}
				Long currentNodeId=-1L;
				this.setNodeId(this.getNodeId()==null?defaultNodeId:this.getNodeId());
				if(this.getNodeId().toString().trim().toUpperCase().equals(this.defaultNodeId)){
					String tmp=getRequest().getParameter("nodeId");
					if(!StringUtil.isEmpty(tmp)){
						if(StringUtils.isNumeric(tmp)){
							currentNodeId=Long.parseLong(tmp);
						}
					}
				}else{
					if(StringUtils.isNumeric(this.getNodeId().toString())){
						currentNodeId=Long.parseLong(this.getNodeId().toString());
					}
				}
				//上面代码的作用是获取当前传来的NodeId，可以是从地址栏传来的（nodeId参数的值为空即不传值或者为OWNER），也可以是
				//具体的ID
				//siteID有三种可能：1、地址栏传来的 2、设置的 3、从nodeId获取
				INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
				Long  currentSiteId=null;
				if(StringUtil.isEmpty(siteId))
					siteId="currentSite";
				if(siteId.trim().toUpperCase().equals("CURRENTSITE")){
					NodeEntity ne=nodeService.getEntityById(currentNodeId);
					if(ne!=null){
						currentSiteId=ne.getSubSiteId();
					}else{
						currentSiteId=null;
					}
				}else if(siteId.trim().toUpperCase().equals("OWNER")){
					String tmp=getRequest().getParameter("siteId");
					if(!StringUtil.isEmpty(tmp)){
						if(StringUtils.isNumeric(tmp)){
							currentSiteId=Long.parseLong(tmp);
						}
					}
				}else if(StringUtils.isNumeric(siteId)){
					currentSiteId=Long.parseLong(siteId);
				}
			List<NodeExtEntity> nodeList=nodeService.getChildNodeList(currentSiteId,currentNodeId,currentPageSize,currentPageIndex,"id",orderType);
			nodeList=orderList(nodeList);
			if(nodeList.size()>0){
				setItems(nodeList);
				setIte(nodeList.iterator());
			}else{
				setItems(null);
				setIte(null);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private List<NodeExtEntity> orderList(List<NodeExtEntity> list){
		ListSortUtil<NodeExtEntity> sortList = new ListSortUtil<NodeExtEntity>(); 
		sortList.sort(list, orderField, orderType);
		return list;
	}
	public static String getDefaultNodeId() {
		return defaultNodeId;
	}
	public static void setDefaultNodeId(String defaultNodeId) {
		NodeEachTag.defaultNodeId = defaultNodeId;
	}
	public Object getNodeId() {
		return nodeId;
	}
	public void setNodeId(Object nodeId) {
		this.nodeId = nodeId;
	}
	public String getVar() {
		return var;
	}
	public void setVar(String var) {
		this.var = var;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public Integer getCurrentPageSize() {
		return currentPageSize;
	}
	public void setCurrentPageSize(Integer currentPageSize) {
		this.currentPageSize = currentPageSize;
	}
	public Integer getCurrentPageIndex() {
		return currentPageIndex;
	}
	public void setCurrentPageIndex(Integer currentPageIndex) {
		this.currentPageIndex = currentPageIndex;
	}

}
